import { Injectable } from '@angular/core';
import { BaseService } from '../common/base.service';

@Injectable({
  providedIn: 'root'
})
export class HeroesServiceBackend extends BaseService{
  // Service hereda baseservice
  async getAll() {
    try {
      this.setEndPoint('heroes/all');
      return this.get();
    } catch (error) {
      throw error;
    }
  }
  async getSingle(id) {
    try {
      this.setEndPoint(`heroes/single/${id}`);
      return this.get();
    } catch (error) {
      throw error;
    }
  }

  async createHeroe(obj) {
    try {
      this.setEndPoint('editheroes/new');
      return this.post(obj);
    } catch (error) {
      throw error;
    }
  }

  async updateSingle(id,obj) {
    try {
      this.setEndPoint(`editheroes/update/${id}`);
      return this.put(obj);
    } catch (error) {
      throw error;
    }
  }

  async deleteSingle(id) {
    try {
      this.setEndPoint(`editheroes/delete/${id}`);
      return this.delete();
    } catch (error) {
      throw error;
    }
  }
}