import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  urlServer = environment.url
  endpoint = '';
  constructor(private http : HttpClient, private router : Router) { }


  private getHTTPOptions() {
    let httpOptions = {};
    if(sessionStorage.getItem('auth')){
      httpOptions = {
        headers : new HttpHeaders({
          Authorization : `Bearer ${JSON.parse(sessionStorage.getItem('auth')).JWT}`
        })
      }
    }
    return httpOptions;
  }

  processError(obj) {
    if(obj.status == 401) {
      this.router.navigate(['login'])
    } else if(obj.status == 404) {
      
    }
  }

  setEndPoint(endpoint) {
    this.endpoint = endpoint;
  }
  
  async get(queryParams = null){
    const options = this.getHTTPOptions()
    try {
      return await this.http.get(`${this.urlServer}${this.endpoint}`,options).toPromise();
    } catch (error){
      console.log(error);
      this.processError(error);
    }
  }

  async post(obj){
    try {
      const options = this.getHTTPOptions()
      return await this.http.post(`${this.urlServer}${this.endpoint}`,obj,options).toPromise();
    } catch (error) {
      console.log(error);
      this.processError(error);
    }
  }

  async put(obj){
    try {
      const options = this.getHTTPOptions()
      return await this.http.put(`${this.urlServer}${this.endpoint}`,obj,options).toPromise();
    } catch (error) {
      console.log(error);
      this.processError(error);
    }
  }

  async delete(){
    try {
      const options = this.getHTTPOptions()
      return await this.http.delete(`${this.urlServer}${this.endpoint}`,options).toPromise();
    } catch (error) {
      console.log(error);
      this.processError(error);
    }
  } 

}

