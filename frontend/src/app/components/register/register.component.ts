import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/common/auth.service';
import { Router } from '@angular/router'
import { SwalService } from 'src/app/services/common/swal.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form : FormGroup;
  isLoaded : boolean;

  constructor(private authServices : AuthService,
              private swalServices : SwalService,
              private router : Router ) { }

  createForm() {
    this.isLoaded = false;
    this.form = new FormGroup({
      nombre :new FormControl('',[Validators.required]),
      apellido :new FormControl('',[Validators.required]),
      email :new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required]),
      password_confirm : new FormControl('',[Validators.required])
    });
    this.isLoaded = true;
  }

  ngOnInit() {
    this.createForm();
  }

  async auth(){
    let responce = await this.authServices.login(this.form.value);
    let responce_converted = JSON.stringify(responce);
    let result = JSON.parse(responce_converted);

    let storage = {
      email : result.email,
      JWT : result.JWT
    }
    
    sessionStorage.setItem('auth',JSON.stringify(storage))
    this.router.navigate(['heroes'])
  }

  async enviarFormulario(){
    let resultado = await this.authServices.createUser(this.form.value);
    let resultado_string = JSON.stringify(resultado);
    let resultado_json = JSON.parse(resultado_string);

    if(resultado_json.status) {
      this.swalServices.normalMessage({icon : 'success', html : '<b>Usuario registrado correctamente </b>'})
      this.auth()
    } else {
      this.swalServices.normalMessage({icon : 'error', html : '<b>Upps!: <br> El usuario no pudo registrarse correctamente </b>'})
    }

  }

  login(){
    this.router.navigate(['login'])
  }
}
