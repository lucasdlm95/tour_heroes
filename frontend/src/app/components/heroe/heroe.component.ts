import { HeroesServiceBackend } from './../../services/backend/heroes.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SwalService } from 'src/app/services/common/swal.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styleUrls: ['./heroe.component.css']
})
export class HeroeComponent implements OnInit {
  isLogged : boolean = false;
  id_heroe : any;
  heroe : any = {}; 
  route : any;
  constructor(
    private activatedRoute : ActivatedRoute,
    private router : Router,
    private heroesServiceBack : HeroesServiceBackend,
    private swalService : SwalService
    ) {}

  async ngOnInit(){
    this.checkIsLogged()
    this.id_heroe = this.activatedRoute.snapshot.params.id;
    await this.getHeroe(this.id_heroe);
  }

  async getHeroe(id) {
    let result : any = await this.heroesServiceBack.getSingle(id);
    console.log(result)
    this.heroe = result.heroe;
  }

  editHeroe() {
    this.router.navigate([`new-heroe/${this.id_heroe}`])
  }

  async deleteHeroe() {
    let resp = await this.swalService.confirmMessage({text: 'Deseas eliminar este heroe?'});
    if (resp.value) {
      let resultado = await this.heroesServiceBack.deleteSingle(this.id_heroe);
      if (resultado) {
        this.swalService.normalMessage({icon: 'success',html: 'Heroe Eliminado!'});
        this.router.navigate(['heroes'])
      } else {
        this.swalService.normalMessage({icon: 'error',html: 'Fallo al eliminar el heroe!'});
        this.router.navigate([`heroe/${this.id_heroe}`])
      }
    } 
  }

  checkIsLogged() {
    sessionStorage.getItem('auth')? this.isLogged = true: this.isLogged=false
  }
}
