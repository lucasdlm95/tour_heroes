import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroeComponent } from './heroe.component';


const routes: Routes = [
  {path : 'heroe/:id', component : HeroeComponent, data : {showNavbar : true}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroeRoutingModule { }
