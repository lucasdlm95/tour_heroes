import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HeroesServiceBackend } from './../../services/backend/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SwalService } from 'src/app/services/common/swal.service';

@Component({
  selector: 'app-nuevo-heroe',
  templateUrl: './nuevo-heroe.component.html',
  styleUrls: ['./nuevo-heroe.component.css'],
})
export class NuevoHeroeComponent implements OnInit {
  form: FormGroup;
  isLoaded: boolean;
  reason: string;
  message: string;
  heroeObject: any = {
    nombre: '',
    bio: '',
    casa: '',
    aparicion: '',
    img : ''
  };
  heroe: any = {};

  constructor(
    private heroesServiceBack: HeroesServiceBackend,
    private activatedRoute: ActivatedRoute,
    private router : Router,
    private swalService: SwalService
  ) {}

  // /heroes/1
  createForm(obj) {
    this.isLoaded = false;
    this.form = new FormGroup({
      nombre: new FormControl(obj.nombre, [
        Validators.required,
        Validators.minLength(2),
      ]),
      bio: new FormControl(obj.bio, [Validators.required]),
      casa: new FormControl(obj.casa, Validators.required),
      aparicion: new FormControl(obj.aparicion, Validators.required),
      img: new FormControl(obj.img),
    });
    this.isLoaded = true;
  }

  async ngOnInit() {
    this.reason = this.activatedRoute.snapshot.params.reason;
    if (this.reason == 'new') {
      this.message = 'Agregar';
      this.createForm(this.heroeObject);
    } else {
      this.message = 'Editar';
      await this.getHeroes(this.reason);
      this.createForm(this.heroe);
    }
  }

  async enviarFormulario() {
    if (this.reason == 'new') {
      let resp = await this.swalService.confirmMessage({text: 'Deseas dar de alta este heroe?'});
      if (resp.value) {
        let resultado = await this.heroesServiceBack.createHeroe(this.form.value);
        if (resultado) {
          this.swalService.normalMessage({icon: 'success',html: 'Heroe dado de alta!'});
        }
      } else {
        this.form.reset();
      }
    } else {
      let resp = await this.swalService.confirmMessage({text: 'Deseas editar este heroe?'});
      if (resp.value) {
        let resultado = await this.heroesServiceBack.updateSingle(this.reason,this.form.value);
        if (resultado) {
          this.swalService.normalMessage({icon: 'success',html: 'Heroe modificado!'});
          this.router.navigate([`heroe/${this.reason}`])
        }
      } else {
        this.form.reset();
      }
    }
  }

  async getHeroes(id) {
    let result: any = await this.heroesServiceBack.getSingle(id);
    this.heroe = result.heroe;
  }
}
