import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NuevoHeroeComponent } from './nuevo-heroe.component'; 
import { AuthGuard } from 'src/app/guardians/auth.guard';

const routes: Routes = [
  {path : 'new-heroe/:reason', canActivate : [AuthGuard], component:NuevoHeroeComponent, data : {showNavbar : true}}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NuevoHeroeRoutingModule { }
