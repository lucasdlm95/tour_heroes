import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NuevoHeroeRoutingModule } from './nuevo-heroe-routing.module';
import { NuevoHeroeComponent } from './nuevo-heroe.component';


@NgModule({
  declarations: [
    NuevoHeroeComponent
  ],
  imports: [
    CommonModule,
    NuevoHeroeRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class NuevoHeroeModule { }
