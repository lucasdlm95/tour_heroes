import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/common/auth.service';
import { Router } from '@angular/router'
import { SwalService } from 'src/app/services/common/swal.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form : FormGroup;
  loginForm = {
    email: new FormControl('',[Validators.required,Validators.email]),
    password : new FormControl('',[Validators.required])
  }
  isLoaded : boolean;

  constructor(private authServices : AuthService,
              private router : Router,
              private swalServices : SwalService) { }

  createForm() {
    this.isLoaded = false;
    this.form = new FormGroup(this.loginForm);
    this.isLoaded = true;
  }
  ngOnInit() {
    this.createForm();
  }

  async auth(){
    let responce = await this.authServices.login(this.form.value);
    let responce_converted = JSON.stringify(responce);
    let result = JSON.parse(responce_converted);

    let storage = {
      email : result.email,
      JWT : result.JWT
    }

    if (result.status) {
      sessionStorage.setItem('auth',JSON.stringify(storage))
      this.router.navigate(['heroes'])
    } else {
      this.swalServices.normalMessage({icon : 'error', html : '<b>Usuario o contraseña incorrecta </b>'})
      this.form.reset
    }
  }

  register() {
    this.router.navigate(['register'])
  }

}