import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeroeModule } from './components/heroe/heroe.module';
import { HeroesModule } from './components/heroes/heroes.module';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from './layout/layout.module';
import { LoginModule } from './components/login/login.module';
import { RegisterModule } from './components/register/register.module';
import { NuevoHeroeComponent } from './components/nuevo-heroe/nuevo-heroe.component';
import { NuevoHeroeModule } from './components/nuevo-heroe/nuevo-heroe.module';

@NgModule({
  declarations: [
    AppComponent  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LayoutModule,
    HeroeModule,
    HeroesModule,
    LoginModule,
    RegisterModule,
    NuevoHeroeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
