import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLogged : boolean = false;
  leftRoutes : any = [];
  rigthRoutes : any = [];
  constructor(private router : Router) {}

  ngOnInit() {

    this.checkIsLogged();
    this.leftRoutes = [
      {link : 'new-heroe/new', description : 'Crear heroe', icon : '', showIfLoaded: true},
    ]
    this.rigthRoutes = [ 
      {link : 'login', description : 'Login', icon : '', showIfLoaded: false},
      {link : 'register', description : 'Register', icon : '', showIfLoaded: false}
    ]

  }

  logout() {
    localStorage.clear();
    sessionStorage.clear();
    this.router.navigate(['login'])
  }

  checkIsLogged() {
    sessionStorage.getItem('auth')? this.isLogged = true: this.isLogged=false
  }
}
