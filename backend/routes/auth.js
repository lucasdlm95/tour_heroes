const express = require('express');
const router = express.Router();
const userModel = require('../models/userModel');

const uuid = require('node-uuid');
const sha1 = require('sha1');
const jwt = require('jsonwebtoken');
const fs = require('fs');

router.post('/', async (req,res) => {
    try {
        console.log(req.body)
        email = req.body.email;
        password = req.body.password;
        const result = await userModel.logUser(email, sha1(password));

        if(result != undefined) {
            let payload = {id : result._id, email : email}; 
            const privateKey = fs.readFileSync('./claves/privada.pem','utf-8');
            let signOptions = {
                expiresIn : '2h',
                algorithm : "RS256"
            }
            const token = jwt.sign(payload,privateKey,signOptions);

            res.json({status : true, email : email, JWT : token})
        } else {
            res.json({status : false, message : 'unauthorized', JWT : null})
        }
    } catch (error) {
        console.log(error)
        res.sendStatus(500);
    }
})

module.exports = router;