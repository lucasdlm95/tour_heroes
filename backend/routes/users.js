const express = require('express');
const router = express.Router();
const userModel = require('../models/userModel');

const uuid = require('node-uuid');
const sha1 = require('sha1');
const jwt = require('jsonwebtoken');
const fs = require('fs');

router.post('/new', async (req,res) => {
    try {
        obj = {
            nombre : req.body.nombre,
            apellido : req.body.apellido,
            email : req.body.email,
            password : sha1(req.body.password),
            id : uuid()
        };
        const result = await userModel.createUser(obj)
        res.json({status: true, message: 'Nuevo user creado'});
    } catch (error) {
        res.sendStatus(500);
    }
})

module.exports = router;
