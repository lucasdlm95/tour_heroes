const express = require('express');
const router = express.Router();
const heroesModel = require('../models/heroesModel');

router.get('/single/:id', async (req,res) => {
    try {
        const result = await heroesModel.getHeroe(req.params.id);
        res.json({heroe : result});
    } catch (error) {
        console.log(error)
        res.sendStatus(500);
    }
})

router.get('/all', async (req,res) => {
    try {
        const result = await heroesModel.getHeroes();
        res.json({heroes : result});
    } catch (error) {
        console.log(error)
        res.sendStatus(500);
    }
})

module.exports = router;