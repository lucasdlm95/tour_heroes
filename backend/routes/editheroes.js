const express = require('express');
const router = express.Router();
const heroesModel = require('../models/heroesModel');

router.post('/new', async (req,res) => {
    try {
        const result = await heroesModel.createHeroe(req.body)
        res.json({status: true, message: 'Nuevo heroe creado'});
    } catch (error) {
        res.sendStatus(500);
    }
})

router.put('/update/:id', async (req,res) => {
    try {
        const result = await heroesModel.updateHeroe(req.params.id,req.body)
        res.json({status: true, message: 'Heroe modificado'});
    } catch (error) {
        res.sendStatus(500);
    }
})

router.delete('/delete/:id', async (req,res) => {
    try {
        const result = await heroesModel.deleteHeroe(req.params.id)
        res.json({status: true, message: 'Heroe eliminado'});
    } catch (error) {
        res.sendStatus(500);
    }
})

module.exports = router;