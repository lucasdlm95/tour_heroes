const jwt = require('jsonwebtoken');
const fs = require('fs');

  securedUser = async (req,res,next) => {
    try {
      let token = req.headers.authorization; // token que envia el usuario
      token = token.replace('Bearer ','');
      const publicKey = fs.readFileSync('./claves/publica.pem');
      let decoded = jwt.verify(token,publicKey);
      req.id = decoded.id;
      req.email = decoded.email;
      next()
    } catch(error) {
      console.log(error);
      res.status(401).json({status : false, message : 'unauthorized'})
    }
  }

  module.exports = {securedUser};