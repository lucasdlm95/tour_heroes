const dbModel = require('../routes/utils/db');
const ObjectId = require('mongodb').ObjectID;

getUser = async (id) => {
    try {
        let filter = {nombre : 1, bio : 1, aparicion : 1, img : 1, casa : 1, id : 1, _id : 0}
        const dbo = await dbModel.pool();
        const collection = await dbo.collection(process.env.C_USER)
                                    .findOne(
                                        {id : id},
                                        {projection : filter}
                                    );
        return collection;
    } catch (error) {
        throw error;
    }
}

logUser = async (email, password) => {
    try {
        let filter = {nombre : 1, apellido : 1, email: 1, id : 1, _id : 0}
        const dbo = await dbModel.pool();
        const collection = await dbo.collection(process.env.C_USER)
                                    .findOne(
                                        {email : email, password : password},
                                        {projection : filter}
                                    );
        console.log({email : email, password : password})
        return collection;
    } catch (error) {
        throw error;
    }
}

createUser = async (obj) => {
    try {
        const dbo = await dbModel.pool();
        const collection = await dbo.collection(process.env.C_USER)
                                    .insertOne(obj);
        return collection;
    } catch (error) {
        throw error;
    }
}

module.exports = {getUser, logUser, createUser}